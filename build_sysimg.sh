#!/bin/bash

# Create AOSP system images containing F-Droid Privileged Extension
# aosp_x86_64-eng + privileged-extension in priv-app

set -e

PROG_DIR=$(dirname $(realpath $0))
TMP_DIR=$(mktemp -d -t fdroidsysimg.tmp.XXXXXXXX)
trap "rm -rf $TMP_DIR" EXIT

function error() {
	echo "*** ERROR: " $@
	usage
}

function usage() {
	cat << EOFU
Usage: $0 aospdir arch outputdir
where:
- aospdir is path to aosp source
- arch is one of arm, arm64, x86, x86_64
- outputdir will contain the zip and sys-img.xml
EOFU
	exit 1
}

AOSP_DIR="$1"
[[ -z "$AOSP_DIR" ]] && error "Missing AOSP directory"

ARCH="$2"
[[ -z "$ARCH" ]] && error "Missing arch"

DEST="$3"
[[ -z "$DEST" ]] && error "Missing output directory"
mkdir -p $DEST

case $ARCH in
arm|arm64|x86|x86_64)
	LUNCH_TARGET=aosp_$ARCH-eng
	;;
*)
	error "Invalid arch $ARCH"
esac

pushd $AOSP_DIR
[[ ! -f build/envsetup.sh ]] && error "Missing build/envsetup.sh in $AOSP_DIR"
source ~/.bashrc-android #cde - temp, specific to my setup
source build/envsetup.sh
lunch $LUNCH_TARGET
# TODO Ensure privileged extension is present
mmm -j8 packages/apps/privileged-extension/app/src/main/
make -j$(cat /proc/cpuinfo | grep ^processor | wc -l) $OUT/{system.img,userdata.img,ramdisk.img}
# TODO Build kernel, and ship prebuilts as below:
for f in $OUT/{system.img,userdata.img,ramdisk.img,obj/NOTICE.txt,system/build.prop} prebuilts/qemu-kernel/$ARCH/kernel-qemu; do
	cp $f $TMP_DIR/
done
[[ -f prebuilts/qemu-kernel/$ARCH/ranchu/kernel-qemu ]] && cp prebuilts/qemu-kernel/$ARCH/ranchu/kernel-qemu $TMP_DIR/kernel-ranchu
[[ $ARCH == "arm" ]] && cp prebuilts/qemu-kernel/$ARCH/kernel-qemu-armv7 $TMP_DIR/kernel-qemu

$PROG_DIR/create_package.sh $TMP_DIR $DEST

popd
